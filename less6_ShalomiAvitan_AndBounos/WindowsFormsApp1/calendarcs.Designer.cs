﻿namespace WindowsFormsApp1
{
    partial class calendarcs
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.birthdayCalendarcs = new System.Windows.Forms.MonthCalendar();
            this.chekToWhoHave = new System.Windows.Forms.Label();
            this.checkBirthday = new System.Windows.Forms.Label();
            this._addDate = new System.Windows.Forms.DateTimePicker();
            this.userNameAdd = new System.Windows.Forms.Label();
            this._userNameAdd = new System.Windows.Forms.TextBox();
            this.Add = new System.Windows.Forms.Button();
            this.Exit = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // birthdayCalendarcs
            // 
            this.birthdayCalendarcs.Location = new System.Drawing.Point(18, 39);
            this.birthdayCalendarcs.Name = "birthdayCalendarcs";
            this.birthdayCalendarcs.TabIndex = 0;
            this.birthdayCalendarcs.DateChanged += new System.Windows.Forms.DateRangeEventHandler(this.monthCalendar1_DateChanged);
            // 
            // chekToWhoHave
            // 
            this.chekToWhoHave.AutoSize = true;
            this.chekToWhoHave.Location = new System.Drawing.Point(62, 214);
            this.chekToWhoHave.Name = "chekToWhoHave";
            this.chekToWhoHave.Size = new System.Drawing.Size(0, 13);
            this.chekToWhoHave.TabIndex = 1;
            // 
            // checkBirthday
            // 
            this.checkBirthday.AutoSize = true;
            this.checkBirthday.Location = new System.Drawing.Point(15, 214);
            this.checkBirthday.Name = "checkBirthday";
            this.checkBirthday.Size = new System.Drawing.Size(0, 13);
            this.checkBirthday.TabIndex = 2;
            // 
            // _addDate
            // 
            this._addDate.CustomFormat = "mm -DD- yyyy";
            this._addDate.Location = new System.Drawing.Point(320, 67);
            this._addDate.Name = "_addDate";
            this._addDate.Size = new System.Drawing.Size(209, 20);
            this._addDate.TabIndex = 3;
            this._addDate.ValueChanged += new System.EventHandler(this.dateTimePicker1_ValueChanged);
            // 
            // userNameAdd
            // 
            this.userNameAdd.AutoSize = true;
            this.userNameAdd.Location = new System.Drawing.Point(426, 39);
            this.userNameAdd.Name = "userNameAdd";
            this.userNameAdd.Size = new System.Drawing.Size(103, 13);
            this.userNameAdd.TabIndex = 4;
            this.userNameAdd.Text = ":שם החוגג להוספה";
            // 
            // _userNameAdd
            // 
            this._userNameAdd.Location = new System.Drawing.Point(320, 36);
            this._userNameAdd.Name = "_userNameAdd";
            this._userNameAdd.Size = new System.Drawing.Size(100, 20);
            this._userNameAdd.TabIndex = 5;
            // 
            // Add
            // 
            this.Add.Location = new System.Drawing.Point(320, 93);
            this.Add.Name = "Add";
            this.Add.Size = new System.Drawing.Size(209, 44);
            this.Add.TabIndex = 6;
            this.Add.Text = "הוסף";
            this.Add.UseVisualStyleBackColor = true;
            this.Add.Click += new System.EventHandler(this.Add_Click);
            // 
            // Exit
            // 
            this.Exit.Location = new System.Drawing.Point(18, 2);
            this.Exit.Name = "Exit";
            this.Exit.Size = new System.Drawing.Size(75, 23);
            this.Exit.TabIndex = 7;
            this.Exit.Text = "יציאה";
            this.Exit.UseVisualStyleBackColor = true;
            this.Exit.Click += new System.EventHandler(this.button1_Click);
            // 
            // calendarcs
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(578, 233);
            this.Controls.Add(this.Exit);
            this.Controls.Add(this.Add);
            this.Controls.Add(this._userNameAdd);
            this.Controls.Add(this.userNameAdd);
            this.Controls.Add(this._addDate);
            this.Controls.Add(this.checkBirthday);
            this.Controls.Add(this.chekToWhoHave);
            this.Controls.Add(this.birthdayCalendarcs);
            this.Name = "calendarcs";
            this.Text = "calendarcs";
            this.Load += new System.EventHandler(this.calendarcs_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MonthCalendar birthdayCalendarcs;
        private System.Windows.Forms.Label chekToWhoHave;
        private System.Windows.Forms.Label checkBirthday;
        private System.Windows.Forms.DateTimePicker _addDate;
        private System.Windows.Forms.Label userNameAdd;
        private System.Windows.Forms.TextBox _userNameAdd;
        private System.Windows.Forms.Button Add;
        private System.Windows.Forms.Button Exit;
    }
}