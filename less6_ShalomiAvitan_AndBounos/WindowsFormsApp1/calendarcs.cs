﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class calendarcs : Form 
    {
        private string _userName;
        public calendarcs(string user)
        {
            InitializeComponent();
            _userName = user;

        }

        //the function check if on this date fron the file have event or no
        //if NewUser =true he check if have brithey if nor he add the new dateof the event - bounos
        private void OpenFile(object sender, string dateUser,bool newUser,string newName)
        {
            string path = @"C:\Users\magshimim\Downloads\" + _userName + "BD.txt";
            if (File.Exists(path))
            {
                try
                {
                    StreamReader sr = new StreamReader(path);
                    String line = "";
                    string input;
                    string dateFile;
                    string name;
                    bool check = true;

                    while (line != null && check)
                    {
                        // Read the stream to a string, and write the string to the console.
                        line = sr.ReadLine();

                        if (line != null)
                        {
                            input = line;
                            name = input.Split(',').First();
                            dateFile = input.Split(',').Last();



                            if (dateUser == dateFile)
                            {
                                checkBirthday.Text = " חוגג יום הולדת!  – " + name + " בתאריך הנבחר";
                                if(newUser)//if get new people from user.
                                {
                                    MessageBox.Show("בתאריך הזה חוגגים כבר יום הולדת");
                                    newUser = false;
                                }
                                check = false;
                            }

                        }
                    }
                    if(check)//if is true dont have one in day
                    {
                        checkBirthday.Text = "L אף אחד לא חוגג יום הולדת ";
                        if(newUser)
                        {
                            using (StreamWriter stream = new StreamWriter(path, true))
                            {
                                stream.WriteLine(newName + "," + dateUser);
                            }
                            MessageBox.Show("היום הולדת שלך הוספה בהצלחה");
                        }
                    }
                }
                catch
                {
                    MessageBox.Show("שגיאה");

                }
            }
            else
            {
                try
                {
                    // Create the new file.

                    FileStream fs = File.Create(path);
                    {
                        Byte[] info = new UTF8Encoding(true).GetBytes("");
                        // Add some information to the file.
                        fs.Write(info, 0, info.Length);
                    }
                    OpenFile(sender, dateUser, newUser, newName);

                }
                catch
                {

                }
            }
        }


        private void monthCalendar1_DateChanged(object sender, DateRangeEventArgs e)
        {
            string dathUser = birthdayCalendarcs.SelectionStart.ToString();
            string [] newDateFormat;
            string input = dathUser;
            string date;
            input = input.Split(' ').First();
            newDateFormat = input.Split('/').ToArray();
            if (Int32.Parse(newDateFormat[0]) / 10 == 0) // check if the day is in 1>9
            {
                newDateFormat[0] = newDateFormat[0].Replace("0", "");
            }
            if (Int32.Parse(newDateFormat[1])/10 == 0) // check if the month is in 1>9
            {
                newDateFormat[1] = newDateFormat[1].Replace("0", "");
            }
            date = newDateFormat[1] + "/" + newDateFormat[0] + "/" + newDateFormat[2];//to get format DD-MM-YYYY


            OpenFile(sender, date,false,"");//is false because is noe get new people to list
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }
        //the bounos add people 
        private void Add_Click(object sender, EventArgs e)
        {
            string dathUser = _addDate.Value.ToString();
            string[] newDateFormat;
            string input = dathUser;
            string date;
            input = input.Split(' ').First();
            newDateFormat = input.Split('/').ToArray();
            if (Int32.Parse(newDateFormat[0]) / 10 == 0) // check if the day is in 1>9
            {
                newDateFormat[0] = newDateFormat[0].Replace("0", "");
            }
            if (Int32.Parse(newDateFormat[1]) / 10 == 0) // check if the month is in 1>9
            {
                newDateFormat[1] = newDateFormat[1].Replace("0", "");
            }
            date = newDateFormat[1] + "/" + newDateFormat[0] + "/" + newDateFormat[2];//to get format DD-MM-YYYY

            OpenFile(sender, date, true, _userNameAdd.Text);//is true because is get new people to list

            calendarcs clean = new calendarcs(_userName);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void calendarcs_Load(object sender, EventArgs e)
        {

        }
    }
}
