﻿namespace WindowsFormsApp1
{
    partial class loginScrenn
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._userText = new System.Windows.Forms.TextBox();
            this._passText = new System.Windows.Forms.TextBox();
            this._userName = new System.Windows.Forms.Label();
            this._password = new System.Windows.Forms.Label();
            this._exit = new System.Windows.Forms.Button();
            this._login = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // _userText
            // 
            this._userText.Location = new System.Drawing.Point(187, 34);
            this._userText.Name = "_userText";
            this._userText.Size = new System.Drawing.Size(100, 20);
            this._userText.TabIndex = 0;
            this._userText.TextChanged += new System.EventHandler(this.userText_TextChanged);
            // 
            // _passText
            // 
            this._passText.Location = new System.Drawing.Point(187, 77);
            this._passText.Name = "_passText";
            this._passText.Size = new System.Drawing.Size(100, 20);
            this._passText.TabIndex = 1;
            this._passText.TextChanged += new System.EventHandler(this.passText_TextChanged);
            // 
            // _userName
            // 
            this._userName.AutoSize = true;
            this._userName.Location = new System.Drawing.Point(324, 40);
            this._userName.Name = "_userName";
            this._userName.Size = new System.Drawing.Size(68, 13);
            this._userName.TabIndex = 2;
            this._userName.Text = ":שם משתמש";
            this._userName.Click += new System.EventHandler(this.label1_Click);
            // 
            // _password
            // 
            this._password.AutoSize = true;
            this._password.Location = new System.Drawing.Point(323, 83);
            this._password.Name = "_password";
            this._password.Size = new System.Drawing.Size(44, 13);
            this._password.TabIndex = 3;
            this._password.Text = ":סיסמא";
            this._password.Click += new System.EventHandler(this.label2_Click);
            // 
            // _exit
            // 
            this._exit.Location = new System.Drawing.Point(272, 137);
            this._exit.Name = "_exit";
            this._exit.Size = new System.Drawing.Size(95, 23);
            this._exit.TabIndex = 4;
            this._exit.Text = "ביטול";
            this._exit.UseVisualStyleBackColor = true;
            this._exit.Click += new System.EventHandler(this.button1_Click);
            // 
            // _login
            // 
            this._login.Location = new System.Drawing.Point(135, 136);
            this._login.Name = "_login";
            this._login.Size = new System.Drawing.Size(99, 23);
            this._login.TabIndex = 5;
            this._login.Text = "כניסה";
            this._login.UseVisualStyleBackColor = true;
            this._login.Click += new System.EventHandler(this.button2_Click);
            // 
            // loginScrenn
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(402, 172);
            this.Controls.Add(this._login);
            this.Controls.Add(this._exit);
            this.Controls.Add(this._password);
            this.Controls.Add(this._userName);
            this.Controls.Add(this._passText);
            this.Controls.Add(this._userText);
            this.Name = "loginScrenn";
            this.Text = "loginScrenn";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox _userText;
        private System.Windows.Forms.TextBox _passText;
        private System.Windows.Forms.Label _userName;
        private System.Windows.Forms.Label _password;
        private System.Windows.Forms.Button _exit;
        private System.Windows.Forms.Button _login;
    }
}

